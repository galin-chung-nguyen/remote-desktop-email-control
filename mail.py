import multiprocessing
import threading;
import json;

import imaplib
import smtplib
import email

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

from os.path import basename, exists;
import os;
import io;

import command;
import gui;
from sync import threadLock;

def getProgramFolderPath():
  return './';
    # if not os.path.exists('./Remote desktop email control.cfg'): return './';
    # f = io.open('./Remote desktop email control.cfg', 'r', encoding='utf8');
    # return f.readline();

def str2Bool(str):
  if type(str) == bool: return str;
  return str.lower().capitalize() == "True";

class MailReplying:
  def __init__(self, msgId, mailFrom, mailSubject, mailBody, attFiles = []):
    self.msgId = msgId;
    self.mailFrom = mailFrom;
    self.mailBody = mailBody;
    self.mailSubject = mailSubject;
    self.attFiles = attFiles;
    # self.isReplied = False;
  
  # def setReplied(self, b):
  #   self.isReplied = b;
  
  # def isReplied(self):
  #   return self.isReplied;
  
  def destroyAttFiles(self):
    for f in self.attFiles:
      if exists(f): os.remove(f);
    self.attFiles = [];
  
  def getMsgID(self):
    return self.msgId;

  def getMailFrom(self):
    return self.mailFrom;
  
  def getSubject(self):
    return self.mailSubject;
  
  def getMailBody(self):
    return self.mailBody;

  def getAttFiles(self):
    return self.attFiles;

  def serialize(self):
    obj = {
      'msgId': self.msgId,
      'mailFrom': self.mailFrom,
      'mailSubject': self.mailSubject,
      'mailBody': self.mailBody,
      'attFiles': self.attFiles
    };
    return json.dumps(obj);
  
  def deserialize(str):
    obj = json.loads(str);
    return MailReplying(obj['msgId'],obj['mailFrom'],obj['mailSubject'],obj['mailBody'],obj['attFiles']);

class MailHandler(threading.Thread):
  WORKSPACE_FILE_PATH = 'cache/mails.json';
  instance = None;
  
  def getInstance():
    if MailHandler.instance == None:
      MailHandler();
    return MailHandler.instance;

  def __init__(self):
    threading.Thread.__init__(self);
    if MailHandler.instance != None:
      MailHandler.instance.__destroy();
    MailHandler.instance = self;

    self.imapHost = 'imap.gmail.com'
    self.imapPort = imaplib.IMAP4_SSL_PORT;
    self.imapRequiredSSL = True;
    self.imapRequiredTLS = False;
    self.smtpHost = 'smtp.gmail.com'
    self.smtpPort = 587;
    self.smtpRequiredSSL = False;
    self.smtpRequiredTLS = True;
    self.portalMailUser = 'hdnham02@gmail.com'
    self.portalMailPass = 'euqrfjdshidkxcft'

    self.commanderMails = [ 'huynhducnham@gmail.com',
                            'dorakid2002@gmail.com' ];
    self.imap = None;
    
    self.requestMailIDs = [];
    self.repList = [];

    self.guiUpdateSettingQueue = multiprocessing.Queue();

    # self.__connectMailServer();
    self.isRunning = True;
    self.isStopping = True;# Stop until connect to mail server
    
    #
    self.wsFilePath = os.path.join(getProgramFolderPath(), MailHandler.WORKSPACE_FILE_PATH);

    self.loadWorkspace();
  
  def start(self):
    super().start();
    return self.guiUpdateSettingQueue;
  
  def updateSetting(self, adminMails, controllerServerSetting):
    self.__disconnectMailServer();

    # update
    self.commanderMails = adminMails;
    self.smtpHost = controllerServerSetting['smtpHost'];
    self.smtpPort = 587 if controllerServerSetting['smtpPort'] == '' else int(controllerServerSetting['smtpPort']);
    self.smtpRequiredSSL = str2Bool(controllerServerSetting['smtpRequiredSSL']);
    self.smtpRequiredTLS = str2Bool(controllerServerSetting['smtpRequiredTLS']);
    self.imapHost = controllerServerSetting['imapHost'];
    self.imapPort = imaplib.IMAP4_SSL_PORT if controllerServerSetting['imapPort'] == '' else int(controllerServerSetting['imapPort']);
    self.imapRequiredSSL = str2Bool(controllerServerSetting['imapRequiredSSL']);
    self.imapRequiredTLS = str2Bool(controllerServerSetting['imapRequiredTLS']);
    self.portalMailUser = controllerServerSetting['username'];
    self.portalMailPass = controllerServerSetting['password'];

    try:
      self.__connectMailServer();
      print('Connected to IMAP Server!');
      gui.GUIProcess.getInstance().appendText2Monitor('Connected to IMAP Server!');
    except Exception:
      self.imap = None;
      print('Cannot connect to IMAP Server.........');
      gui.GUIProcess.getInstance().appendText2Monitor('Cannot connect to IMAP Server.........');

  def checkUpdateSetting(self):
    if not self.guiUpdateSettingQueue.empty():
      adminMails, controllerServerSetting = self.guiUpdateSettingQueue.get_nowait();
      self.updateSetting(adminMails, controllerServerSetting);

  def debugSetting(self):
    print('Admin mails:');
    for m in self.commanderMails:
      print('-', m);
    print('--------------------');
    print('Smtp Host:',self.smtpHost);
    print('Smtp Port:',self.smtpPort);
    print('Smtp Required SSL:',self.smtpRequiredSSL);
    print('Smtp Required TLS:',self.smtpRequiredTLS);
    print('--------------------');
    print('Imap Host:',self.imapHost);
    print('Imap Port:',self.imapPort);
    print('Imap Required SSL:',self.imapRequiredSSL);
    print('Imap Required TLS:',self.imapRequiredTLS);
    print('--------------------');
    print('Username:',self.portalMailUser);
    print('Password:',self.portalMailPass);
  
  def __destroy(self):
    self.__disconnectMailServer();
    self.saveWorkspace();
    print('mail thread was finished');
  
  def __disconnectMailServer(self):
    if self.imap == None: return;
    self.isStopping = True;
    self.imap.close();
    self.imap = None;
  
  def __connectMailServer(self):
    self.imap = (imaplib.IMAP4_SSL(self.imapHost, self.imapPort) if self.imapRequiredSSL else imaplib.IMAP4(self.imapHost, self.imapPort));
    if self.imapRequiredTLS: self.imap.starttls();
    self.imap.login(self.portalMailUser, self.portalMailPass);
    self.debugSetting();
    self.isStopping = False;

  def run(self):
    while self.isRunning:
      self.checkUpdateSetting();
      if not self.isStopping:
        self.__searchNewMail();
        self.__fetchNextMail();
        self.__replyNextMail();
    self.__destroy();
  
  def __replyNextMail(self):
    if len(self.repList) == 0: return;
    mailReplying = self.repList[0];
    isReplied = self.__replyMail(mailReplying.getMsgID(), mailReplying.getMailFrom(), mailReplying.getSubject(), mailReplying.getMailBody(), mailReplying.getAttFiles());
    if isReplied:
      mailReplying.destroyAttFiles();
      threadLock.acquire();
      self.repList.pop(0); # remove from repList
      threadLock.release();
      print('Replied to', mailReplying.getMailFrom(), ',', mailReplying.getSubject(), ':', mailReplying.getMailBody());

  def __replyMail(self, msgId, mailFrom, mailSubject, mailBody, files=[]):
    new = MIMEMultipart("mixed")
    body = MIMEMultipart("alternative")
    body.attach( MIMEText(mailBody, "plain") )
    new.attach(body)

    new["Message-ID"] = email.utils.make_msgid();
    new["In-Reply-To"] = msgId;
    new["References"] = msgId;
    new["Subject"] = "Re: " + mailSubject;
    new["To"] = mailFrom;
    new["From"] = self.portalMailUser;

    for f in files:
      if not exists(f):
        continue;
      print('prepare sending file:', f);
      with open(f, "rb") as fil:
        part = MIMEApplication(fil.read(), Name=basename(f));
        # After the file is closed
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f);
        new.attach(part);

    try:
      session = (smtplib.SMTP_SSL(self.smtpHost, self.smtpPort) if self.smtpRequiredSSL else smtplib.SMTP(self.smtpHost, self.smtpPort));
      if self.smtpRequiredTLS: session.starttls();
      session.login(self.portalMailUser, self.portalMailPass)
      session.sendmail(self.portalMailUser, [new["To"]], new.as_string())
      session.quit();
    except Exception:
      print('Cannot connect to SMTP Server.........');
      gui.GUIProcess.getInstance().appendText2Monitor('Cannot connect to SMTP Server.........');
      return False;
    return True;
  
  def __fetchMail(self, mailId):
    typ, data = self.imap.fetch(mailId,'(RFC822)');
    for responsePart in data:
      if isinstance(responsePart, tuple):
        msg = email.message_from_string(responsePart[1].decode("utf-8"));

        msgId = msg['Message-ID'];
        mailSubject = msg['subject'];
        mailFrom = msg['from'].split('<')[1].split('>')[0];

        body = "";
        if msg.is_multipart():
          for part in msg.walk():
            ctype = part.get_content_type()
            cdispo = str(part.get('Content-Disposition'))

            # skip any text/plain (txt) attachments
            if ctype == 'text/plain' and 'attachment' not in cdispo:
              body = part.get_payload(decode=True);
              break
        # not multipart - i.e. plain text, no attachments, keeping fingers crossed
        else:
          body = msg.get_payload(decode=True);
        return (msgId, mailFrom, mailSubject, body.decode().replace("\r", ""));
    return (None, None, None, None);
  
  def __searchNewMail(self):
    self.imap.select('Inbox');# For refesh too
    tmp, data = self.imap.search(None, 'UnSeen')
    mailIds = data[0].decode();
    self.requestMailIDs = mailIds.split(' ');
    # print('Search new mail:', self.requestMailIDs);
    if self.requestMailIDs[0] == '': self.requestMailIDs = [];
  
  def __fetchNextMail(self):
    if len(self.requestMailIDs) == 0: return;
    threadLock.acquire();
    mailID = self.requestMailIDs.pop(0);
    threadLock.release();
    msgId, mailFrom, mailSubject, mailBody = self.__fetchMail(mailID);
    if msgId == None or (mailFrom not in self.commanderMails):
      self.__fetchNextMail();# Run again if cannot fetch current mail or mailFrom is not a commander
      return;
    # parse to command elements
    cmd = mailSubject;
    params = {};
    rawParams = mailBody.split('\n');
    rawParams = list(filter(lambda e: ':' in e, rawParams));
    for p in rawParams:
      entry = p.split(':');
      key = (entry.pop(0)).strip();
      value = (":".join(entry)).strip();
      params[key] = value;
    print(mailFrom, 'send', cmd, 'command with parameters:', params);
    command.CommandHandler.getInstance().runCommand(msgId, mailFrom, cmd, params);

  def pushReplyMail(self, mailReplying):
    if not self.isStopping and self.isRunning:
      threadLock.acquire();
      self.repList.append(mailReplying);
      threadLock.release();
      return True;
    return False;
  
  def saveWorkspace(self):
    serRepList = [];
    for rep in self.repList:
      serRepList.append(rep.serialize());
    obj = {'requestMailIDs': self.requestMailIDs, 'serRepList': serRepList};
    #
    file = open(self.wsFilePath, 'wt');
    json.dump(obj, file);
    file.close();
  
  def loadWorkspace(self):
    if not os.path.exists(self.wsFilePath): return;
    file = open(self.wsFilePath, 'r');
    try:
      obj = json.load(file);
      self.requestMailIDs = obj['requestMailIDs'];
      serReqList = obj['serRepList'];
      for serReq in serReqList:
        self.repList.append(MailReplying.deserialize(serReq));
    except ValueError:
      pass;
    file.close();
    os.remove(self.wsFilePath);

  def stop(self):
    self.isStopping = True;
    self.isRunning = False;

###### Test ###########
# MailHandler.getInstance().start();