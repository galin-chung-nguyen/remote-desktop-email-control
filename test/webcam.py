# import cv2
# cap = cv2.VideoCapture(2)
# cap.set(3,640)
# cap.set(4,480)

# cv2.

# # out = cv2.VideoWriter('output.mp4',0x7634706d , 20.0, (640,480))

# while(True):
#     ret, frame = cap.read()
#     # out.write(frame)
#     cv2.imshow('frame', frame)
#     c = cv2.waitKey(1)

#     cv2.imwrite("test_capture_webcam.png", frame)

#     break
#     if c & 0xFF == ord('q'):
#         break

# cap.release()
# # out.release()
# cv2.destroyAllWindows()


# # # Python program to illustrate 
# # # saving an operated video
  
# # # organize imports
# # import numpy as np
# # import cv2
  
# # # This will return video from the first webcam on your computer.
# # cap = cv2.VideoCapture(2)  
  
# # # Define the codec and create VideoWriter object
# # fourcc = cv2.VideoWriter_fourcc(*'XVID')
# # out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640, 480))
  
# # # loop runs if capturing has been initialized. 
# # while(True):
# #     # reads frames from a camera 
# #     # ret checks return at each frame
# #     ret, frame = cap.read() 
  
# #     # Converts to HSV color space, OCV reads colors as BGR
# #     # frame is converted to hsv
# #     hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
      
# #     # output the frame
# #     out.write(hsv) 
      
# #     # The original input frame is shown in the window 
# #     cv2.imshow('Original', frame)
  
# #     # The window showing the operated video stream 
# #     cv2.imshow('frame', hsv)
  
      
# #     # Wait for 'a' key to stop the program 
# #     if cv2.waitKey(1) & 0xFF == ord('a'):
# #         break
  
# # # Close the window / Release webcam
# # cap.release()
  
# # # After we release our webcam, we also release the output
# # out.release() 
  
# # # De-allocate any associated memory usage 
# # cv2.destroyAllWindows()

# # # import cv2 
# # # def check():
# # #     index = 0
# # #     arr = []
# # #     while True:
# # #         cap = cv2.VideoCapture(index)
# # #         if not cap.read()[0]:
# # #             break
# # #         else:
# # #             arr.append(index)
# # #         cap.release()
# # #         index += 1
# # #     return arr

# # # print(check())

# # # import os
# # # devs = os.listdir('/dev')
# # # vid_indices = [int(dev[-1]) for dev in devs 
# # #                if dev.startswith('video')]
# # # vid_indices = sorted(vid_indices)
# # # print(vid_indices)

import cv2
from datetime import datetime, timezone
import os
import time 

webcamCaptureFolder = "testWebcam"
videoLength = 0

if(not os.path.exists(webcamCaptureFolder)):
    os.makedirs(webcamCaptureFolder)
try:
    # Capture image
    if(videoLength <= 0):
    
        cap = cv2.VideoCapture(2)
        cap.set(3,640)
        cap.set(4,480)
        outputFile = '{}.png'.format(datetime.utcnow().replace(tzinfo=datetime.now().astimezone().tzinfo).strftime("Webcam-image %d-%m-%Y_%H-%M-%S_%z"))

        while(True):
            ret, frame = cap.read()
            # out.write(frame)
            cv2.imshow('frame', frame)
            c = cv2.waitKey(1)

            cv2.imwrite(os.path.join(webcamCaptureFolder, outputFile), frame)

            break
            if c & 0xFF == ord('q'):
                break
        cap.release()
        cv2.destroyAllWindows()
    else:
        #         cap = cv2.VideoCapture(0,cv2.CAP_DSHOW)
        # cap.set(3,1920)
        # cap.set(4,1080)
        # out = cv2.VideoWriter('output.avi',fourcc, 20.0, (1920,1080))

        outputFile = '{}.mp4'.format(datetime.utcnow().replace(tzinfo=datetime.now().astimezone().tzinfo).strftime("Webcam-video %d-%m-%Y_%H-%M-%S_%z"))
        print("ok -> ",outputFile)

        cap = cv2.VideoCapture(2)
        cap.set(3,640)
        cap.set(4,480)
        out = cv2.VideoWriter(os.path.join(webcamCaptureFolder, outputFile),0x7634706d , 20.0, (640,480))
        startTime = time.time()
        while(True):
            ret, frame = cap.read()
            out.write(frame)
            cv2.imshow('frame', frame)

            endTime = time.time()
            if(endTime - startTime >= videoLength):
                break
        cap.release()
        out.release()
        cv2.destroyAllWindows()
except Exception as e:
    pass