from concurrent.futures import thread
import os
import psutil
import time
from datetime import datetime
import math

# print(psutil)
logPath = os.path.abspath(os.getcwd()) + '/some/path/proclogs'
if not os.path.exists(logPath):
    os.makedirs(logPath)

separator = "-" * 80
format = "%7s %7s %12s %12s %30s, %s"
format2 = "%7.4f %7.2f %12s %12s %30s, %s"
# for proc in psutil.process_iter():
#     # print('process {} -> {}'.format(proc.pid, proc.name()))
#     print(proc)

result = 'List of processes:\n'
for proc in psutil.process_iter():
    # pid=99936, name='com.apple.quicklook.ThumbnailsAgent', status='running', started='2022-06-06 17:23:23')
    # print('new Process #{} {}', proc.pid, proc.name())
    
    pid = "#"
    name = "#"
    status = "#"
    create_times = "#"
    cpu_times = "#"
    cpu_percent = "#"
    num_threads = "#"
    user = "#"

    try:
        pid = proc.pid 
        name = proc.name()
        status = proc.status()
        create_times = datetime.fromtimestamp(proc.create_time()).strftime("%A, %B %d, %Y %I:%M:%S")
        num_threads = proc.num_threads()
        cpu_times = sum(proc.cpu_times()[:2])

        cpu_minutes = math.floor(cpu_times)
        cpu_hours = math.floor(cpu_times)//60
        cpu_minutes -= cpu_hours * 60
        cpu_seconds = math.ceil((cpu_times - cpu_hours * 60 - cpu_minutes) * 60)

        cpu_times = '{}:{}:{}'.format(cpu_hours, cpu_minutes, cpu_seconds)

        cpu_percent = proc.cpu_percent()
        user = proc.username()
    except Exception as e:
        print(e)
        pass
    curProcess = "Process #{}: name='{}', status='{}', started='{}', cpu_times ='{}', cpu_percent='{}', num_threads='{}', user='{}'".format(pid, name, status, create_times, cpu_times, cpu_percent, num_threads, user)
    result += curProcess + "\n"

print(result)

print('----------------------------------------------------')
print('----------------------------------------------------')
print('----------------------------------------------------')

while True:
    print("Which process you want to stop/resume/kill? ")
    cmd, pid = input().split()
    pid = int(pid)

    if(cmd.lower() == 'stop'):
        for proc in psutil.process_iter():
            if(proc.pid == pid):
                print('Are you sure you want to stop process ({}, {})?'.format(pid, proc.name()))
                result = input()
                if(result.strip().lower() == "y"):
                    proc.suspend()
    elif (cmd.lower() == 'kill'):
        for proc in psutil.process_iter():
            if(proc.pid == pid):
                print('Are you sure you want to kill process ({}, {})?'.format(pid, proc.name()))
                result = input()
                if(result.strip().lower() == "y"):
                    proc.kill()
    elif (cmd.lower() == 'resume'):
        for proc in psutil.process_iter():
            if(proc.pid == pid):
                print('Are you sure you want to resume process ({}, {})?'.format(pid, proc.name()))
                result = input()
                if(result.strip().lower() == "y"):
                    proc.resume()
    
    else:
        print('Stop')
        break
    # cpu_percent = proc.cpu_percent()
    # mem_percent = proc.memory_percent()
    # rss, vms = proc.memory_info()
    # rss = str(rss)
    # vms = str(vms)
    # name = proc.name
    # path = proc.path
    # print(format2 % (cpu_percent, mem_percent, vms, rss, name, path))
    # print("\n\n")    


# while 1:
#     procs = psutil.get_process_list()
#     procs = sorted(procs, key=lambda proc: proc.name)
    
#     logPath = r'some\path\proclogs\procLog%i.log' % int(time.time())
#     f = open(logPath, 'w')
#     f.write(separator + "\n")
#     f.write(time.ctime() + "\n")
#     f.write(format % ("%CPU", "%MEM", "VMS", "RSS", "NAME", "PATH"))
#     f.write("\n")
    
#     for proc in procs:
#         cpu_percent = proc.get_cpu_percent()
#         mem_percent = proc.get_memory_percent()
#         rss, vms = proc.get_memory_info()
#         rss = str(rss)
#         vms = str(vms)
#         name = proc.name
#         path = proc.path
#         f.write(format2 % (cpu_percent, mem_percent, vms, rss, name, path))
#         f.write("\n\n")
#     f.close()
#     print("Finished log update!")
#     time.sleep(300)
#     print("writing new log data!")