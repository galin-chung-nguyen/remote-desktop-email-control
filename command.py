import multiprocessing
import threading;
import json;
import os;
import io;

import mail;
from action import ActionHandlerManager;
import gui;
from sync import threadLock;

def getProgramFolderPath():
  return './';
    # if not os.path.exists('./Remote desktop email control.cfg'): return './';
    # f = io.open('./Remote desktop email control.cfg', 'r', encoding='utf8');
    # return f.readline();

class CommandRequest:
  def __init__(self, msgId, mailFrom, cmd, params):
    self.status = 'init'; # init/requesting/resulted/error/done
    self.mail = {'msgId': msgId, 'from': mailFrom};
    self.cmd = cmd;
    self.params = params;
    self.replyBody = {'reply_msg': '', 'attached_files': []};
  
  def __setStatus(self, s):
    self.status = s;
  
  def getStatus(self):
    return self.status;

  def getMsgID(self):
    return self.mail['msgId'];
  
  def getFrom(self):
    return self.mail['from'];

  def getParam(self, key):
    return self.params[key] if key in self.params else '';
  
  def getCMD(self):
    return self.cmd;
  
  def process(self):
    if self.status == 'init':
      # send to ActionHandler for handling
      if ActionHandlerManager.getInstance().handle(self) == True:
        threadLock.acquire();
        self.status = 'requesting';
        threadLock.release();
    elif self.status == 'resulted' or self.status == 'error':
      # send reply
      if self.__reply() == True:
        threadLock.acquire();
        self.status = 'done';
        threadLock.release();
    return self.status;
  
  def __reply(self):
    if self.mail['from'] == 'console':
      gui.GUIProcess.getInstance().appendText2Monitor('Rep: '+str(self.cmd));
      gui.GUIProcess.getInstance().appendText2Monitor(str(self.replyBody['reply_msg']));
      gui.GUIProcess.getInstance().appendText2Monitor('Att: '+str(self.replyBody['attached_files']));
      return True;
    else:
      mailReplying = mail.MailReplying(self.mail['msgId'], self.mail['from'], self.cmd, self.replyBody['reply_msg'], self.replyBody['attached_files']);
      return mail.MailHandler.getInstance().pushReplyMail(mailReplying);
  
  def response(self, result):
    threadLock.acquire();
    if result['status'] == 'reback':
      self.status = 'init';
    else: # result['status'] == done/error
      self.status = 'error' if result['status'] == 'error' else 'resulted';
      self.replyBody['reply_msg'] = result['reply_msg'];
      self.replyBody['attached_files'] = result['attached_files'];
    threadLock.release();
  
  def serialize(self):
    jsonObj = {
      'class': 'CommandRequest',
      'body': {
        'status': self.status,
        'mail': self.mail,
        'cmd': self.cmd,
        'params': self.params,
        'replyBody': self.replyBody
      }
    }
    return json.dumps(jsonObj);
  
  def deserialize(str):
    jsonObj = json.loads(str);
    if 'class' not in jsonObj or 'body' not in jsonObj or jsonObj['class'] != 'CommandRequest': return None;
    status = jsonObj['body']['status'];
    mail = jsonObj['body']['mail'];
    cmd = jsonObj['body']['cmd'];
    params = jsonObj['body']['params'];
    replyBody = jsonObj['body']['replyBody'];
    cmdReq =  CommandRequest(mail['msgId'], mail['from'], cmd, params);
    cmdReq.__setStatus(status);
    cmdReq.replyBody = replyBody;
    return cmdReq;

class CommandHandler(threading.Thread):
  WORKSPACE_FILE_PATH = 'cache/command.json';
  instance = None;

  def getInstance():
    if CommandHandler.instance == None:
      CommandHandler.instance = CommandHandler();
    return CommandHandler.instance;

  def __init__(self):
    threading.Thread.__init__(self);
    if CommandHandler.instance != None:
      CommandHandler.instance.__destroy();
    CommandHandler.instance = self;
    #
    self.reqList = [];
    #
    self.guiCmdQueue = multiprocessing.Queue();
    #
    self.isRunning = True;
    #
    self.wsFilePath = os.path.join(getProgramFolderPath(), CommandHandler.WORKSPACE_FILE_PATH);

    self.loadWorkspace();
  
  def start(self):
    super().start();
    return self.guiCmdQueue;

  def __destroy(self):
    self.saveWorkspace();
    print('command thread was finished');

  def saveWorkspace(self):
    serReqList = [];
    for req in self.reqList:
      serReqList.append(req.serialize());
    #
    file = open(self.wsFilePath, 'wt');
    json.dump(serReqList, file);
    file.close();
  
  def loadWorkspace(self):
    if not os.path.exists(self.wsFilePath): return;
    file = open(self.wsFilePath, 'r');
    try:
      serReqList = json.load(file);
      for serReq in serReqList:
        self.reqList.append(CommandRequest.deserialize(serReq));
    except ValueError:
      pass;
    file.close();
    os.remove(self.wsFilePath);

  def runCommand(self, msgId, mailFrom, cmd, params):
    print(msgId, ' ', mailFrom, ' ', cmd, ' ' ,params)
    cmdReq = CommandRequest(msgId, mailFrom, cmd, params);
    threadLock.acquire();
    self.reqList.append(cmdReq);
    threadLock.release();

  def checkCmdFromGUI(self):
    if not self.guiCmdQueue.empty():
      cmd, params = self.guiCmdQueue.get_nowait();
      self.runCommand('console', 'console', cmd, params);

  def run(self):
    while self.isRunning:
      self.checkCmdFromGUI();
      i = 0;
      for cmdReq in self.reqList:
        if cmdReq.process() == 'done': self.reqList.pop(i);
        i += 1;
    self.__destroy();

  def stop(self):
    self.isRunning = False;

###### Test ###########
# CommandHandler.getInstance().start();