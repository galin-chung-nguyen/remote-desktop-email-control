# RDEC

Remote Desktop Email Control - RDEC is a program that allows users to perform some operations on a remote computer by sending mail. The user will set parameters required for RDEC to identify Portal accounts (accounts that receive control emails) and Commander accounts (accounts that are allowed to send control emails). Commander accounts can compose emails complying the pre-defined syntax to send commands control to the Portal account. RDEC will rely on the parameter set to access and retrieve control emails and perform corresponding actions on the device running the program.

## Demo images

### Overview
![Login screen](./assets/readme/overviewRDEC.png)
![Login screen](./assets/readme/overviewRDEC2.png)
![Life time](./assets/readme/lifetime.png)

### User flow
![Compose email](./assets/readme/composeEmail.png)
![Compose email](./assets/readme/launch.png)
![Login screen](./assets/readme/replyMail.png)

## Using RDEC

To run RDEC:
- 1. Install `python`
- 2. Install all the required modules: `pip install -r requirements.txt`
- 3. Run the main program: `python main.py`
- 4. Config Portal email and Commander emails, then `Run`.

## Contributors
Thanks to the following people who have contributed to this project:

* [@loc4atnt](https://github.com/loc4atnt) 📖
* [@trislee02](https://github.com/trislee02) 🐛

## Contact

If you want to contact me you can reach out to me at [Linkedin](https://www.linkedin.com/in/galin-chung-nguyen/).

### **Have a good day!**

<!-- ## License

This project uses the following license: [<license_name>](<link>). -->
<!-- # Donus

Donus is a free web-based messaging app for everyone. It's built using React, Redux and Firebase storage. You can use it to make chat group with your friends, send and receive messages instantly, do cool things with it. Feel free to discover and enjoy it!

#### Login screen

![Login screen](./assets/readme/demoScreen_logIn.png)

#### Chat

![Chat](./assets/readme/chat.png)

#### Invite new friends to the chat

![Invite new friends to the chat](./assets/readme/invite-friend.png)

#### Join a chat

![Join a chat](./assets/readme/inviteScreen.png)

#### Chat member settings

![Chat member settings](./assets/readme/change-role.png)

You can check it out here: https://donus-chat.web.app/

### **Have a good day!**
 -->
