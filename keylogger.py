# key logging
from audioop import mul
from pynput.keyboard import Listener;
from datetime import datetime;
import multiprocessing;

class KeyLoggerProcess(multiprocessing.Process):
    def __init__(self):
        multiprocessing.Process.__init__(self);
        self.isRunning = multiprocessing.Value('i', 0);
        self.keyQueue = multiprocessing.Queue();
    
    def start(self):
        super().start();
        self.isRunning.value = 1;
        return self.keyQueue;
    
    def on_press(self, key):
        keyWithTime = [datetime.now(), key];
        print('{}: {}'.format(keyWithTime[0].ctime(), key))
        self.keyQueue.put_nowait(keyWithTime);

    def run(self):
        self.keyListener = Listener(on_press=self.on_press);
        self.keyListener.start();
        while self.isRunning.value == 1: pass;
        self.keyListener.stop();
        print('keylogger_process was finished')
    
    def stop(self):
        self.isRunning.value = 0;