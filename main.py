import gui;
import mail;
import command;
import action
import os;
import io;

def getProgramFolderPath():
  return './';
    # if not os.path.exists('./Remote desktop email control.cfg'): return './';
    # f = io.open('./Remote desktop email control.cfg', 'r', encoding='utf8');
    # return f.readline();

class Main:
  instance = None;

  def getInstance():
    if Main.instance == None:
      Main();
    return Main.instance;

  def __init__(self):
    if Main.instance != None:
      Main.instance.__destroy();
    Main.instance = self;
  
  def __destroy(self):
    action.ActionHandlerManager.getInstance().checkPowerActionAfterDestroyApp();
  
  def start(self):
    print('Program folder path:', getProgramFolderPath());
    print('Setup program............');
    self.__setup();
    #
    print('Loading..................');
    self.myGUI.start();# start and run GUI
    while self.myGUI.isRunning.value:
      action.ActionHandlerManager.getInstance().loop();
      pass;
    #
    self.stop();
    #
    print('Destroy program..........');
    self.__destroy();
    #
    print('.........................');
  
  def __setup(self):
    # Register Action Handler by CMD (and run Instance too)
    actHandlerMnger = action.ActionHandlerManager.getInstance();
    actHandlerMnger.registerAnActionHandler('LIST_PROCESSES', action.ListProcessesActionHandler());#1
    actHandlerMnger.registerAnActionHandler('STOP_PROCESS', action.StopProcessActionHandler());#2
    actHandlerMnger.registerAnActionHandler('RESUME_PROCESS', action.ResumeProcessActionHandler());#3
    actHandlerMnger.registerAnActionHandler('KILL_PROCESS', action.KillProcessActionHandler());#4
    actHandlerMnger.registerAnActionHandler('TAKE_SCREENSHOT', action.TakeScreenShotActionHandler());#5
    actHandlerMnger.registerAnActionHandler('GET_REGISTRY', action.GetRegistryActionHandler());#7
    actHandlerMnger.registerAnActionHandler('SET_REGISTRY', action.SetRegistryActionHandler());#7
    actHandlerMnger.registerAnActionHandler('COPY_FILE', action.CopyFileActionHandler());#7
    actHandlerMnger.registerAnActionHandler('WEBCAM', action.WebcamActionHandler());#7
    actHandlerMnger.registerAnActionHandler('KEY_LOGGER', action.KeyLoggerActionHandler());#9
    actHandlerMnger.registerAnActionHandler('PING', action.PingActionHandler());#1
    actHandlerMnger.registerAnActionHandler('???', action.InvalidActionHandler());#3
    actHandlerMnger.registerAnActionHandler('SHUTDOWN', action.ShutdownActionHandler());#4
    actHandlerMnger.registerAnActionHandler('GET_FILE', action.GetFileActionHandler());#4

    # Call start to the modules
    guiCmdQueue = command.CommandHandler.getInstance().start();#2
    guiUpdateSettingQueue = mail.MailHandler.getInstance().start();#3
    # self.myGUI = gui.GUI();
    self.myGUI = gui.GUIProcess(guiCmdQueue, guiUpdateSettingQueue);

  def stop(self):
    print('Stop program.............');
    #1
    mail.MailHandler.getInstance().stop();
    mail.MailHandler.getInstance().join();
    #2
    action.ActionHandlerManager.getInstance().stop();
    action.ActionHandlerManager.getInstance().join();
    #3
    command.CommandHandler.getInstance().stop();
    command.CommandHandler.getInstance().join();

##########################
if __name__ == "__main__":
  Main.getInstance().start();